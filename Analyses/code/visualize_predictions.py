#!/anaconda/bin/python

import warnings
warnings.filterwarnings('ignore')
import os
import sys
import joblib
import numpy as np
import pandas as pd
import logging
import time
from math import log

import matplotlib
import matplotlib.pyplot as plt
plt.style.use('default')
import seaborn as sns
import pathlib
import collections 
from itertools import product
from sklearn import metrics
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.externals import joblib
from sklearn.linear_model import LogisticRegression

def get_script_path():
    return os.path.dirname(os.path.realpath(sys.argv[0]))
    
def get_tf_datasets()    :
    dir_script = get_script_path()
    tfids_file = "{}/tfids.tsv".format(dir_script)
    df_tfs = pd.read_csv(tfids_file, sep="\t")
    return(df_tfs["id"].tolist())

def ic_scale(pwm,background):
    odds_ratio = ((pwm+0.001)/(1.004))/(background[None,:])
    ic = ((np.log((pwm+0.001)/(1.004))/np.log(2))*pwm -\
            (np.log(background)*background/np.log(2))[None,:])
    return pwm*(np.sum(ic,axis=1)[:,None])


def plot_a(ax, base, left_edge, height, color):
    a_polygon_coords = [
        np.array([
           [0.0, 0.0],
           [0.5, 1.0],
           [0.5, 0.8],
           [0.2, 0.0],
        ]),
        np.array([
           [1.0, 0.0],
           [0.5, 1.0],
           [0.5, 0.8],
           [0.8, 0.0],
        ]),
        np.array([
           [0.225, 0.45],
           [0.775, 0.45],
           [0.85, 0.3],
           [0.15, 0.3],
        ])
    ]
    for polygon_coords in a_polygon_coords:
        ax.add_patch(
            matplotlib.patches.Polygon(
                (np.array([1,height])[None,:]*polygon_coords
                 + np.array([left_edge,base])[None,:]),
                facecolor=color, edgecolor=color))


def plot_c(ax, base, left_edge, height, color):
    ax.add_patch(matplotlib.patches.Ellipse(
        xy=[left_edge+0.65, base+0.5*height], width=1.3, height=height,
        facecolor=color, edgecolor=color))
    ax.add_patch(matplotlib.patches.Ellipse(
        xy=[left_edge+0.65, base+0.5*height], width=0.7*1.3, height=0.7*height,
        facecolor='white', edgecolor='white'))
    ax.add_patch(matplotlib.patches.Rectangle(
        xy=[left_edge+1, base], width=1.0, height=height,
        facecolor='white', edgecolor='white', fill=True))


def plot_g(ax, base, left_edge, height, color):
    ax.add_patch(matplotlib.patches.Ellipse(
        xy=[left_edge+0.65, base+0.5*height], width=1.3, height=height,
        facecolor=color, edgecolor=color))
    ax.add_patch(matplotlib.patches.Ellipse(
        xy=[left_edge+0.65, base+0.5*height], width=0.7*1.3, height=0.7*height,
        facecolor='white', edgecolor='white'))
    ax.add_patch(matplotlib.patches.Rectangle(
        xy=[left_edge+1, base], width=1.0, height=height,
        facecolor='white', edgecolor='white', fill=True))
    ax.add_patch(matplotlib.patches.Rectangle(
        xy=[left_edge+0.825, base+0.085*height],
        width=0.174, height=0.415*height,
        facecolor=color, edgecolor=color, fill=True))
    ax.add_patch(matplotlib.patches.Rectangle(
        xy=[left_edge+0.625, base+0.35*height],
        width=0.374, height=0.15*height,
        facecolor=color, edgecolor=color, fill=True))


def plot_t(ax, base, left_edge, height, color):
    ax.add_patch(
        matplotlib.patches.Rectangle(xy=[left_edge+0.4, base],
        width=0.2, height=height, facecolor=color, edgecolor=color, fill=True))
    ax.add_patch(
        matplotlib.patches.Rectangle(xy=[left_edge, base+0.8*height],
        width=1.0, height=0.2*height, facecolor=color,
        edgecolor=color, fill=True))

    
def plot_u(ax, base, left_edge, height, color):
    ax.add_patch(matplotlib.patches.Ellipse(
        xy=[left_edge+0.5, base+0.4*height], width=0.95, height=0.8*height,
        facecolor=color, edgecolor=color))
    ax.add_patch(matplotlib.patches.Rectangle(
        xy=[left_edge+0.025, base+0.4*height], width=0.95, height=0.6*height,
        facecolor=color, edgecolor=color, fill=True))
    ax.add_patch(matplotlib.patches.Ellipse(
        xy=[left_edge+0.5, base+0.4*height], width=0.6175, height=0.52*height,
        facecolor='white', edgecolor='white'))
    ax.add_patch(matplotlib.patches.Rectangle(
        xy=[left_edge+0.19125, base+0.4*height], width=0.6175, height=0.6*height,
        facecolor='white', edgecolor='white', fill=True))

    
default_colors = {0:'red', 1:'blue', 2:'orange', 3:'green'}
dna_plot_funcs = {0:plot_a, 1:plot_c, 2:plot_g, 3:plot_t}
rna_plot_funcs = {0:plot_a, 1:plot_c, 2:plot_g, 3:plot_u}


def plot_weights_given_ax(ax, array,          
                 height_padding_factor=0.2,
                 length_padding=1.0,
                 subticks_frequency="auto",
                 colors=default_colors,
                 plot_funcs=dna_plot_funcs,
                 highlight={},
                 ylabel=""):
    if len(array.shape)==3:
        array = np.squeeze(array)
    assert len(array.shape)==2, array.shape
    if (array.shape[0]==4 and array.shape[1] != 4):
        array = array.transpose(1,0)
    assert array.shape[1]==4
    if (subticks_frequency=="auto"):
        subticks_frequency = 1.0 if len(array) <= 40 else int(len(array)/40)
    max_pos_height = 0.0
    min_neg_height = 0.0
    heights_at_positions = []
    depths_at_positions = []
    for i in range(array.shape[0]):
        #sort from smallest to highest magnitude
        acgt_vals = sorted(enumerate(array[i,:]), key=lambda x: abs(x[1]))
        positive_height_so_far = 0.0
        negative_height_so_far = 0.0
        for letter in acgt_vals:
            plot_func = plot_funcs[letter[0]]
            color=colors[letter[0]]
            if (letter[1] > 0):
                height_so_far = positive_height_so_far
                positive_height_so_far += letter[1]                
            else:
                height_so_far = negative_height_so_far
                negative_height_so_far += letter[1]
            plot_func(ax=ax, base=height_so_far,
                      left_edge=i+0.5, height=letter[1], color=color)
        max_pos_height = max(max_pos_height, positive_height_so_far)
        min_neg_height = min(min_neg_height, negative_height_so_far)
        heights_at_positions.append(positive_height_so_far)
        depths_at_positions.append(negative_height_so_far)

    #now highlight any desired positions; the key of
    #the highlight dict should be the color
    for color in highlight:
        for start_pos, end_pos in highlight[color]:
            assert start_pos >= 0.0 and end_pos <= array.shape[0]
            min_depth = np.min(depths_at_positions[start_pos:end_pos])
            max_height = np.max(heights_at_positions[start_pos:end_pos])
            ax.add_patch(
                matplotlib.patches.Rectangle(xy=[start_pos,min_depth],
                    width=end_pos-start_pos,
                    height=max_height-min_depth,
                    edgecolor=color, fill=False))
            
    ax.set_xlim(0.5-length_padding, 0.5+array.shape[0]+length_padding)
    ax.xaxis.set_ticks(np.arange(1.0, array.shape[0]+1, subticks_frequency))
    height_padding = max(abs(min_neg_height)*(height_padding_factor),
                         abs(max_pos_height)*(height_padding_factor))
    ax.set_ylim(min_neg_height-height_padding, max_pos_height+height_padding)
    ax.set_ylabel(ylabel)
    ax.yaxis.label.set_fontsize(15)


def plot_weights(array,
                 figsize=(20,2),
                 ax_transform_func=lambda x: x,
                 **kwargs):
    fig = plt.figure(figsize=figsize)
    ax = fig.add_subplot(111) 
    ax_transform_func(ax)
    plot_weights_given_ax(ax=ax,
        array=array,
        **kwargs)
    plt.show()
    
    
def save_plot_weights(array,
                 figsize=(20,2),
                 ax_transform_func=lambda x: x,
                      idx=0,
                 **kwargs):
    fig = plt.figure(figsize=figsize)
    ax = fig.add_subplot(111) 
    ax_transform_func(ax)
    plot_weights_given_ax(ax=ax,
        array=array,
        **kwargs)
    dir_script = get_script_path()
    data_dir = dir_script.replace("code","data")
    fig.savefig("{}/logo/occlusion_bok_ZmTF{}.pdf".format(data_dir, idx), bbox_inches='tight')
    
    
def dna_plot_weights(array, **kwargs):
    plot_weights(array=array, plot_funcs=dna_plot_funcs, **kwargs)
    
def save_dna_plot_weights(array, idx=0, **kwargs):
    save_plot_weights(array=array, idx=idx, plot_funcs=dna_plot_funcs, **kwargs)

def rna_plot_weights(array, **kwargs):
    plot_weights(array=array, plot_funcs=rna_plot_funcs, **kwargs)
    
def one_hot_encode_along_channel_axis(sequence, scores):
    to_return = np.zeros((len(sequence),4), dtype=np.float)
    seq_to_one_hot_fill_in_array(zeros_array=to_return,
                                 sequence=sequence, scores=scores, one_hot_axis=1)
    return to_return


def seq_to_one_hot_fill_in_array(zeros_array, sequence, scores, one_hot_axis):
    assert one_hot_axis==0 or one_hot_axis==1
    if (one_hot_axis==0):
        assert zeros_array.shape[1] == len(sequence)
    elif (one_hot_axis==1): 
        assert zeros_array.shape[0] == len(sequence)
    #will mutate zeros_array
    for (i,char) in enumerate(sequence):
        if (char=="A" or char=="a"):
            char_idx = 0
        elif (char=="C" or char=="c"):
            char_idx = 1
        elif (char=="G" or char=="g"):
            char_idx = 2
        elif (char=="T" or char=="t"):
            char_idx = 3
        elif (char=="N" or char=="n"):
            continue #leave that pos as all 0's
        else:
            raise RuntimeError("Unsupported character: "+str(char))
        if (one_hot_axis==0):
            zeros_array[char_idx,i] = scores[i]
            #if scores[i] > 0:
                #zeros_array[char_idx,i] = scores[i]
        elif (one_hot_axis==1):
            zeros_array[i,char_idx] = scores[i]
            #if scores[i] > 0: 
                #zeros_array[i,char_idx] = scores[i]    


def predict_for_batch(fasta_seqs, extract_idf, TFIDF_LR):
    df_sequences = pd.DataFrame(fasta_seqs, index=range(len(fasta_seqs[0])), columns =['dna_string']) 
    df_sequences["tokens"] = df_sequences["dna_string"].apply(write_ngrams)
    occluded_tokens = df_sequences["tokens"].tolist()
    occluded_tfidf = extract_idf.transform(occluded_tokens) 
    occlusion_scores = TFIDF_LR.predict_proba(occluded_tfidf )[:,1] # y_score
    return occlusion_scores

def insilico_mutagenesis(input_sequence, extract_idf, TFIDF_LR, isc):
    mutate_plan = {'A':[],'T':[],'C':[],'G':[]}
    for mutate_to in mutate_plan.keys():
        fasta_seqs = [mutate_sequence(mutate_to, input_sequence, pos) for pos in range(len(input_sequence))]
        occlusion_scores = predict_for_batch(fasta_seqs, extract_idf, TFIDF_LR)
        occlusion_differences = [isc - osc  for osc in occlusion_scores]
        mutate_plan[mutate_to] = [0 if i < 0 else i for i in occlusion_differences]
    return pd.DataFrame(mutate_plan)

def mutagenesis_map(df_test, id_plot):
    from matplotlib.colors import Normalize
    class MidpointNormalize(Normalize):
        def __init__(self, vmin=None, vmax=None, midpoint=None, clip=False):
            self.midpoint = midpoint
            Normalize.__init__(self, vmin, vmax, clip)
        def __call__(self, value, clip=None):
            # I'm ignoring masked values and all kinds of edge cases to make a
            # simple example...
            x, y = [self.vmin, self.midpoint, self.vmax], [0, 0.5, 1]
            return np.ma.masked_array(np.interp(value, x, y))
    
    fig, ax = plt.subplots(figsize=(20,2)) 
    norm = MidpointNormalize(midpoint=df_test.max().max())
    sns.heatmap(df_test.transpose(), cmap="Reds", norm=norm, linewidths=.5, ax=ax, cbar=False)
    plt.xticks([])
    dir_script = get_script_path()
    data_dir = dir_script.replace("code","data")
    fig.savefig("{}/mutagenesis/mutagenesis_bok_ZmTF{}.pdf".format(data_dir, id_plot), bbox_inches='tight')
    

def createKmerSet(kmersize):
    '''
    write all possible kmers
    :param kmersize: integer, 8
    :return uniq_kmers: list of sorted unique kmers
    '''
    kmerSet = set()
    nucleotides = ["a", "c", "g", "t"]    
    kmerall = product(nucleotides, repeat=kmersize)
    for i in kmerall:
        kmer = ''.join(i)
        kmerSet.add(kmer)
    uniq_kmers = sorted(list(kmerSet))  
    return uniq_kmers


def compute_kmer_entropy(kmer):
    '''
    compute shannon entropy for each kmer
    :param kmer: string
    :return entropy: float
    '''
    prob = [float(kmer.count(c)) / len(kmer) for c in dict.fromkeys(list(kmer))]
    entropy = - sum([ p * log(p) / log(2.0) for p in prob ])
    return round(entropy, 2)


def make_stopwords(kmersize):
    '''
    write filtered out kmers
    :param kmersize: integer, 8
    :return stopwords: list of sorted low-complexity kmers
    '''
    kmersize_filter = {5:1.3, 6:1.3, 7:1.3, 8:1.3, 9:1.3, 10:1.3}
    limit_entropy = kmersize_filter.get(kmersize)
    kmerSet = set()
    nucleotides = ["a", "c", "g", "t"]    
    kmerall = product(nucleotides, repeat=kmersize)
    for n in kmerall:
        kmer = ''.join(n)
        
        if compute_kmer_entropy(kmer) < limit_entropy:
            kmerSet.add(make_newtoken(kmer))
        else:
            continue
    stopwords = sorted(list(kmerSet))
    return stopwords

  
def createNewtokenSet(kmersize):
    '''
    write all possible newtokens
    :param kmersize: integer, 8
    :return uniq_newtokens: list of sorted unique newtokens
    ''' 
    newtokenSet = set()
    uniq_kmers = createKmerSet(kmersize)
    for kmer in uniq_kmers:
        newtoken = make_newtoken(kmer)
        newtokenSet.add(newtoken)  
    uniq_newtokens = sorted(list(newtokenSet))
    return uniq_newtokens      


def make_newtoken(kmer):
    '''
    write a collapsed kmer and kmer reverse complementary as a newtoken
    :param kmer: string e.g., "AT"
    :return newtoken: string e.g., "atnta"
    :param kmer: string e.g., "TA"
    :return newtoken: string e.g., "atnta"
    '''
    kmer = str(kmer).lower()
    newtoken = "n".join(sorted([kmer,kmer.translate(str.maketrans('tagc', 'atcg'))[::-1]]))
    return newtoken

def write_ngrams(sequence):
    '''
    write a bag of newtokens of size n
    :param sequence: string e.g., "ATCG"
    :param (intern) kmerlength e.g., 2
    :return newtoken_string: string e.g., "atnta" "gatc" "cgcg" 
    '''
    seq = str(sequence).lower()
    finalstart = (len(seq)-kmerlength)+1
    allkmers = [seq[start:(start+kmerlength)] for start in range(0,finalstart)]
    tokens = [make_newtoken(kmer) for kmer in allkmers if len(kmer) == kmerlength and "n" not in kmer]
    newtoken_string = " ".join(tokens)
    return newtoken_string

def tokenize_fasta(file_name, label):
    df_sequences = fasta2corpus(file_name)
    df_sequences["tokens"] = df_sequences["dna_string"].apply(write_ngrams)
    df_sequences["bound"] = label
    return df_sequences["tokens"].tolist(), df_sequences["bound"].tolist(), df_sequences["idx_seq"].tolist()

    
def build_vocabulary(all_tokens, stpwrds):
    tmpvectorizer = TfidfVectorizer(min_df = 1 , max_df = 1.0, sublinear_tf=True,use_idf=True)
    vcblry = tmpvectorizer.get_feature_names()
    kmer_names = [x for x in vcblry if x not in stpwrds]
    feature_names = np.asarray(kmer_names) #key transformation to use the fancy index into the report
        
    #Check that tokens are as many as expected math.pow(4, kmerlength)/2
    if len(kmer_names) > expected_tokens:
        print("ERROR: Expected %d tokens. Obtained %d tokens" % (expected_tokens, len(kmer_names)))
        logging.info("Expecting %d tokens" % expected_tokens)
        logging.info("Feature index contains %d tokens" % len(kmer_names))
        logging.info("ERROR: expected %d tokens, got %d tokens" % (expected_tokens, len(kmer_names)))
        logging.info("ERROR: More features than expected!")
        quit()
    else:
        logging.info("Feature index contains %d tokens" % len(kmer_names))

    return kmer_names, feature_names
    
    
def occluded_sequence(input_sequence, pos):
    original = list(input_sequence)
    original[pos] = 'N'
    return "".join(original)

def mutate_sequence(mutate_to, input_sequence, pos):
    if mutate_to == 'A':
        new_seq = mutated_to_A(input_sequence, pos)
    elif mutate_to == 'C':
        new_seq = mutated_to_C(input_sequence, pos)  
    elif mutate_to == 'G':
        new_seq = mutated_to_G(input_sequence, pos)
    elif mutate_to == 'T':
        new_seq = mutated_to_T(input_sequence, pos)
    else:
        new_seq = occluded_sequence(input_sequence, pos)
    return new_seq
        
def mutated_to_A(input_sequence, pos):
    original = list(input_sequence)
    original[pos] = 'A'
    return "".join(original)

def mutated_to_T(input_sequence, pos):
    original = list(input_sequence)
    original[pos] = 'T'
    return "".join(original)

def mutated_to_C(input_sequence, pos):
    original = list(input_sequence)
    original[pos] = 'C'
    return "".join(original)

def mutated_to_G(input_sequence, pos):
    original = list(input_sequence)
    original[pos] = 'G'
    return "".join(original)    
    
def fasta2corpus(file_name):
    fasta_sequences = collections.OrderedDict()
    fasta_header = None
    with open(file_name) as sequences:
        for line in sequences:
            if line.startswith(">"):
                fasta_header = line.strip().replace(">","")
                fasta_sequences[fasta_header] = []
            else:
                fasta_sequences[fasta_header].append(line.strip())

    for name in fasta_sequences:
        fasta_sequences[name] = ''.join(fasta_sequences[name])

    df_corpus = pd.DataFrame(list(fasta_sequences.values()), columns =['dna_string']) 
    df_corpus['idx_seq'] = list(fasta_sequences.keys())
    return df_corpus
    

def predict_region(df_corpus, tf_number):
    dir_script = get_script_path()
    base_dir = dir_script.replace("code","")
    dir_datasets = "{}/models/{}/".format(dir_script.replace("code", ""), tf_number)  
    transformer_file_path = "{}ZmTF{}_tfidf_transformer.pickle".format(dir_datasets,tf_number)
    trainedmodel_file_path = "{}ZmTF{}.bagkmer.k7.pkl".format(dir_datasets,tf_number)
    extract_idf = joblib.load(transformer_file_path)
    TFIDF_LR = joblib.load(trainedmodel_file_path)
        
    df_corpus["tokens"] = df_corpus["dna_string"].apply(write_ngrams)
    corpus_tokenized = df_corpus["tokens"].tolist()
    corpus_tfidf = extract_idf.transform(corpus_tokenized)
        
    df_corpus['score'] = TFIDF_LR.predict_proba(corpus_tfidf)[:,1] # y_score
    df_corpus['pred'] = np.where(df_corpus.score >= 0.85,1,0) # y_pred adjusted
    df_corpus['tf'] = tf_number
    return df_corpus[['idx_seq','tf','pred','score', 'dna_string']]


def visualize_regions(df_corpus, tf_number):
    from sklearn.externals import joblib
    dir_script = get_script_path()
    dir_datasets = "{}/models/{}/".format(dir_script.replace("code", ""), tf_number)     
    transformer_file_path = "{}ZmTF{}_tfidf_transformer.pickle".format(dir_datasets,tf_number)
    trainedmodel_file_path = "{}ZmTF{}.bagkmer.k7.pkl".format(dir_datasets,tf_number)
    extract_idf = joblib.load(transformer_file_path)
    TFIDF_LR = joblib.load(trainedmodel_file_path)
    df_corpus = df_corpus.loc[df_corpus['pred'] != 0]
    if len(df_corpus.index) > 0:
        for index, row in df_corpus.iterrows():
            id_plot = "{}_{}".format(tf_number,row['idx_seq'])    
            input_sequence = row['dna_string']
            isc = row['score']
            fasta_seqs = [mutate_sequence('N',input_sequence, pos) for pos in range(len(input_sequence))]
            occlusion_scores = predict_for_batch(fasta_seqs, extract_idf, TFIDF_LR)
            occlusion_differences = [isc - osc  for osc in occlusion_scores]
            occlusion_differences_pos = [0 if i < 0 else i for i in occlusion_differences]
            onehot_data = np.array([one_hot_encode_along_channel_axis(x, occlusion_differences_pos) for x in [input_sequence]])
            save_dna_plot_weights(onehot_data[0], idx=id_plot, subticks_frequency=50)


def visualize_mutagenesis_regions(df_corpus, tf_number):
    from sklearn.externals import joblib
    dir_script = get_script_path()
    dir_datasets = "{}/models/{}/".format(dir_script.replace("code", ""), tf_number)    
    transformer_file_path = "{}ZmTF{}_tfidf_transformer.pickle".format(dir_datasets,tf_number)
    trainedmodel_file_path = "{}ZmTF{}.bagkmer.k7.pkl".format(dir_datasets,tf_number)
    extract_idf = joblib.load(transformer_file_path)
    TFIDF_LR = joblib.load(trainedmodel_file_path)
    df_corpus = df_corpus.loc[df_corpus['pred'] != 0]
    if len(df_corpus.index) > 0:
        for index, row in df_corpus.iterrows():
            id_plot = "{}_{}".format(tf_number,row['idx_seq'])       
            input_sequence = row['dna_string']
            isc = row['score']
            df_test = insilico_mutagenesis(input_sequence, extract_idf, TFIDF_LR, isc)
            mutagenesis_map(df_test, id_plot)

if sys.argv[1] == "-help" : #or 
    CRED = '\033[91m'
    CEND = '\033[0m'
    print(CRED + "Usage: python visualize_predictions.py [fasta_file for predictions] [input_tf_id] [visualize_logo] [visualize_mutagenesis_map]" + CEND)
    print(CRED + "Example: python visualize_predictions.py test.fasta 100 True False" + CEND)
    sys.exit()
else: 
    CRED = '\033[91m'
    CEND = '\033[0m'

    family_name = "leaf_ZmTF"
    kmersize = 7
    filtered = True
    mode = "mode_filtered"
    kmerlength = int(kmersize)
    newtoken_size = 1+(kmerlength*2)
    all_tokens = createNewtokenSet(kmerlength)
    expected_tokens = len(all_tokens)
    stpwrds = make_stopwords(kmerlength)
    dir_script = get_script_path()
    base_dir = dir_script.replace("code","")
    data_dir = dir_script.replace("code","data")
    tf_datasets = get_tf_datasets()
    tf_datasets = [x.strip() for x in tf_datasets]
    
    input_fasta = str(sys.argv[1]).strip()
    input_corpus = fasta2corpus(input_fasta)
    input_tf = str(sys.argv[2]).strip()
    input_logo = str(sys.argv[3]).strip().lower()
    input_map = str(sys.argv[4]).strip().lower()

    if input_logo == "true":
        visualize_logo = True
    else:
        visualize_logo = False
    
    if input_map == "true":
        visualize_map = True
    else:
        visualize_map = False
    
    if input_tf in tf_datasets:
        tf_datasets = [input_tf]
        
    pathlib.Path("{}/log".format(base_dir)).mkdir(parents=True, exist_ok=True)
    pathlib.Path("{}/predictions".format(data_dir)).mkdir(parents=True, exist_ok=True)
       
    run_id = str(int(time.time()))
    file_name = '{}/log/kgrammar_bok_model_runid_{}_{}_{}_k{}_{}_logfile.txt'.format(base_dir, run_id, family_name, input_tf, str(kmerlength), mode) 
    logging.basicConfig(level=logging.INFO, filename=file_name, filemode="a+",
                        format="%(asctime)-15s %(levelname)-8s %(message)s")
    logging.info("kmer_grammar_bag-of-k-mers RUN ID")
    logging.info(run_id)
    logging.info("input: kmerlength")
    logging.info(str(kmersize))
    logging.info("input: leaf_ZmTF")
    logging.info(str(input_tf))
    logging.info("input: filtered")
    logging.info(filtered)
    logging.info("input: total input sequences")
    logging.info(str(len(input_corpus.index)))
    logging.info("input: visualize logo")
    logging.info(visualize_logo)
    logging.info("input: visualize mutagenesis map")
    logging.info(visualize_map)
    
    print(CRED + '-' * 80 + CEND)
    print(CRED + "Kgrammar run id: {}".format(run_id) + CEND)
    print(CRED + "Kgrammar %s %s k-size %d -filtered %s -visualize_logo %s -visualize_mutagenesis_map %s" % (family_name, input_tf, kmerlength, str(filtered), str(visualize_logo), str(visualize_map)) + CEND)
    print(CRED + "Saving log file to the log folder ..." + CEND)
    
    logging.info(str("Kgrammar explain"))
    frames = []
    for tf_idx in tf_datasets:
    
        prediction_file = "{}/data/predictions/model_ZmTF{}_prediction_{}.csv".format(base_dir, tf_idx, os.path.basename(input_fasta))
        results_df = predict_region(input_corpus, tf_idx)
        export_csv = results_df[['idx_seq','tf','pred','score']].to_csv(prediction_file, index = None, header=True)
        print(CRED + "Saving prediction file to the prediction folder ..." + CEND)
        pred_category1 = (results_df['pred'] == 1).any()
        
        if pred_category1:
            results_df = results_df.loc[results_df['pred'] != 0]
            npred_category1 = len(results_df.index)
            logging.info("output: total predicted regulatory sequences")
            logging.info(str(npred_category1))
            fasta_pred = round(len(results_df.index) / len(input_corpus.index) * 100, 2)
            print(CRED + "regulatory regions predicted: {}%".format(fasta_pred) + CEND)
            if visualize_logo:
                pathlib.Path("{}/logo".format(data_dir)).mkdir(parents=True, exist_ok=True)
                visualize_regions(results_df, tf_idx)
                print(CRED + "Saving logo plot(s) to the logo folder ..." + CEND)
                logging.info("output: visualize logo")
                logging.info(visualize_logo)
            else:
                logging.info("output: visualize logo")
                logging.info(visualize_logo)
            if visualize_map:
                pathlib.Path("{}/mutagenesis".format(data_dir)).mkdir(parents=True, exist_ok=True)
                visualize_mutagenesis_regions(results_df, tf_idx)
                print(CRED + "Saving Mutagenesis map(s) to the mutagenesis folder ..." + CEND)
                logging.info("output: visualize mutagenesis map")
                logging.info(visualize_map)
            else:
                logging.info("output: visualize mutagenesis map")
                logging.info(visualize_map)
        else:
            visualize_map = False
            visualize_logo = False
            print(CRED + "regulatory regions predicted: 0%" + CEND)
            logging.info("output: total predicted regulatory sequences")
            logging.info("0")
            logging.info("output: visualize logo")
            logging.info(visualize_logo)
            logging.info("output: visualize mutagenesis map")
            logging.info(visualize_map)
