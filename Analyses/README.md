
# Important:
It requires python >=3.5, the full list of requirements are in the **docker/environment.yml** file
You can try to build everything by yourself ...

### To get a reproducible environment you can use:   
1. conda  
2. Docker + conda  

##### Environment Docker + conda
1. Go and install [docker](https://docs.docker.com/)
2. Create a `.env_dev` file with development environment variables

```
# random seed for reproducible models
random_seed=42
```

3. Run `docker-compose build --no-cache`. This will build the development image with all the packages I defined installed within it.
4. Run `docker-compose up` and navigate to your browser to find jupyter server running on [http://localhost:8888](http://localhost:8888). 
5. Access it by entering in the token `test_env`.
6. From insde a notebook you can try:  
  `%run code/visualize_predictions.py -help`  
  `%run code/visualize_predictions.py ../data/test.fasta 100 False False`
7. From insde the terminal you can try:  
  `conda activate base`
  `python code/visualize_predictions.py -help`
  `python code/visualize_predictions.py ../data/test.fasta 100 False False`
8. Once you are done, remember to shutdown jupyter server and docker   
  Go to the terminal and Press Ctrl+C   
  `docker-compose down`

##### Environment conda
1. Go and install [conda](https://docs.anaconda.com/anaconda/install/).
2. `conda env create --name bok_models --file=docker/environments.yml`
3. `conda activate bok_models`  
4. `python code/visualize_predictions.py -help`
5. `python code/visualize_predictions.py ../data/test.fasta 100 False False`

### The models
The models folder MUST be populated. Files live in [`Zenodo`](https://zenodo.org/record/3834199), to see if an individual model is available for yftf (your favorite transcription factor) try    
1. Download the master table with TF ids [tfids](https://zenodo.org/record/3834199/files/tfids.tsv?download=1).  
2. Download the tar.gz file for yftf, for instance if you want the model for `col8`, that correspond to file [`ZmTF2.tar.gz`](https://zenodo.org/record/3834199/files/ZmTF2.tar.gz?download=1).  
3. `tar -xzvf ZmTF2.tar.gz` and move it under `models/`  

### The inputs 
To run the script you need now four arguments:  
1. A fasta file (see example `data/test.fasta`)  
2. The id of one of the 104 TFs - Note: Only ids in the file `code/tfids.tsv` are valid ones  
3. Boolean (True or False) to get a LOGO like plot for the predicted regulatory region  
4. Boolean (True or False) to get a muatgenesis heatmap plot for the predicted regulatory region 
#### Note: It is fast for predictions, and slow for visualization

### The outputs 
1. one csv file with one row for each prediction
  columns in the csv correspond to 
  'idx_seq' -> header from the sequence in the fasta file 
  'tf' -> the id of one of the 104 TFs
  'pred' -> 1 when the TF is predicted to bind, and 0 when is not
  'score' -> The probability score (a value between 0 to 1)

2. An n number of PDF files. Where n is equal to the number of sequences in the fasta file that are predicted as regulatory regions

If you use the models:  
Xiaoyu Tu, María Katherine Mejía-Guerra, Jose A Valdes Franco, David Tzeng, Po-Yu Chu, Xiuru Dai, Pinghua Li, Edward S Buckler, and Silin Zhong (2020). The transcription regulatory code of a plant leaf.
doi:[10.5281/zenodo.3834199](https://doi.org/10.5281/zenodo.3834199)